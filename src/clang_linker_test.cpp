/**
    A sandbox, primarily for testing Visual Studio 2019 and clang linkibg
    with Linden third party libraries but has evolved into a general playground
    for testing clang things.
**/

#include <iostream>

#include "zlib.h"

int main()
{
    const char* ver_str = zlibVersion();
    std::cout << "zlib version is " << ver_str << std::endl;

    std::cout << "Value of __wchar_t is " << sizeof(__wchar_t) << std::endl;
    std::cout << "Value of wchar_t is " << sizeof(wchar_t) << std::endl;

    if (typeid(__wchar_t) == typeid(wchar_t))
    {
        std::cout << "Types are identical" << std::endl;
    }
    else
    {
        std::cout << "Types are NOT identical" << std::endl;
    }
}
